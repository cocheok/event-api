'use strict';

require('dotenv').load();

const app    = require('./app');
const logger = require('./lib/logger');

app.initialize(logger)
    .then((application) => {
        application.listen(process.env.API_SERVER_PORT);
        logger.info('Your server is listening on port ' + process.env.API_SERVER_PORT);
       
    }).catch(err => {
        logger.error(err)
    });
