/* eslint-disable consistent-return */
'use strict';

const logger = require('../logger');

function send(req, res, next) {
    logger.info("called");
    return res.json({ "status": "ok"});
}



module.exports = {
    send
};
